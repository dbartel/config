help:
	@grep -E '^[a-zA-Z_-]+.*:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


all: emacs zsh vim ## Build all configs

emacs:  .FORCE ## Build emacs
	./scripts/build-emacs.sh

emacs-lite: .FORCE ## Build emacs-lite
	emacs --script `pwd`/emacs/scripts/install-lite.el

vim: .FORCE ## Build vim
	./scripts/build-vim.sh

zsh: .FORCE ## Build zsh
	./scripts/build-zsh.sh

.FORCE:



