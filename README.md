My various shell configs

## Software

- zsh
- vim
- emacs

## Installing configs

Run `make all` to install all configs or run `make <application>` to install configs for a specific config
