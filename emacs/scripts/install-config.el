;;; install-config.el --- Installs emacs configuration



;;; Commentary:
;; This script assumes you're running from the Emacs directory of the repository
;; It will:
;;    - Create configuration directories (if necessary)
;;    - Backup existing configuration for easy restoration
;;    - Install the existing configuration into the proper config location
;; example usage: `emacs -script scripts/install-config.el`

;;; Code:

(require 'org)

;; Create config directory if it doesn't exist
(if (file-directory-p "~/.config/emacs") t (make-directory "~/.config/emacs" t))

;; Remove elisp directory if it exists
(if (file-directory-p "~/.config/emacs/elisp")
    (delete-directory "~/.config/emacs/elisp" t)
  t)

;; Only copy environment file if it hasn't been created
(if (file-exists-p "~/.config/emacs-environment.el") t
  (copy-file "./emacs-environment.el" "~/.config/emacs-environment.el"))

;; Create backup of existing configuration
(if (file-exists-p "~/.config/emacs-config.el")
    (copy-file "~/.config/emacs-config.el" "~/.config/emacs-config.el.bk" t)
  nil)

(copy-directory "./elisp" "~/.config/emacs/elisp" nil nil t)

;; Generate emacs config from org file
(org-babel-tangle-file "./emacs-config.org" "~/.config/emacs/emacs-config.el")

(message "Emacs configuration copied to ~/.config/emacs/emacs-config.el")
(message "If you need to restore your old config, a backup has been created at ~/.config.emacs-config.el.bk")

;;; install-config.el ends here
