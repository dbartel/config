;;; install-lite.el --- Installs emacs lite configuration


;;; Commentary:
;; This script assumes you're running from the Emacs directory of the repository
;; It will install the "lite" config
;; This config has 0 dependencies and is meant to be used on a server where

;;;; Code:

(copy-file "./emacs-config-lite.el" "~/.emacs" t)
(message "emasc-config-lite.el copied to .emacs")

;;; install-lite.el ends here
