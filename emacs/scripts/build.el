;;; build.el --- Tests out building the emacs config org file

;;; Commentary:
;; Just loads the file via org-babel-load-file
;;; Code:

(require 'org)

(setq package-user-dir (format "%s/elpa" (getenv "CI_PROJECT_DIR")))

(defun print-error (err)
  "Print an ERR message if it exists."
  (if err
      (progn
	(print err)
	(print (format "Error: [%s] " err)))
    (print "Can't see err"))
  t)

(print ">>>> Loading emacs configuration...")
(unwind-protect
    (let (x)
      (condition-case ex
	  (setq x (org-babel-load-file "./emacs-config.org"))
	('error (progn
		  (print-error ex)
		  (error "Failed to load"))))
      x)
  t)

;;; build.el ends here
