;;; surround-tags-test.el --- tests for surround-tags

;;; Code:
(require 'ert)

(require 'surround-tags)

(ert-deftest surround-tags-test ()
  (ert-test-erts-file "./test/surround-tags.erts"
		      ;; set point to min range
		      ;; activate mark
		      ;; move to max range
		      ;; call surround tags with "
		      (lambda ()
			(move-beginning-of-line nil)
			(set-mark-command nil)
			(move-end-of-line nil)
			(activate-mark)
			(dbartel/surround-tags "*"))))

(provide 'surround-tags-test)

;;; surround-tags-test.el ends here

