;; sanity-test.el --- Sanity test confirming ert is working

;;; Code:

(ert-deftest sanity-test ()
  (should (= 1 1)))

(provide 'sanity-test)

;;; sanity-test.el ends here

