;; all-tests.el --- Wrapper file to run all tests


;;; Code:

;; Load new files if the directory contains compiled files
(setq load-prefer-newer t)

(require 'sanity-test)
(require 'surround-tags-test)

;;; all-tests.el ends here

