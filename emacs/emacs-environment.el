
(setenv "ORG_AGENDA_HOME" "~/org") ;; Directory holding org agenda files
(setenv "FONT_FACE" "-outline-Iosevka-normal-normal-normal-mono-28-*-*-*-c-*-iso8859-1") ;; Default Font
(setenv "DEFT_HOME" "~/.deft") ;; Daft directory
