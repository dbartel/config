;;; surround-tags.el --- surrounds tag in custom region

;;; Commentary:
;; 

;;; Code:

(defun dbartel/surround-tags (tag)
  "Surrounds region in custom TAG."
  (interactive "stag: ")
  (if (region-active-p)
      (let ((beg (region-beginning)))
	(save-excursion
	  (goto-char (region-end))
	  (insert tag)
	  (goto-char beg)
	  (insert tag)))
    (message "Region is not active")))

(provide 'dbartel/surround-tags)

;;; surround-tags.el ends here

(provide 'surround-tags)
