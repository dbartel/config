Emacs configuration

## Environment setup

Modify the values in `emacs-environment.el` to setup machine specific configs

## Custom elisp

Add any custom elisp into the `elisp` dir
