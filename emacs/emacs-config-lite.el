;;; Minimal emacs config meant for running via cli

;; IDO
(require 'ido)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(setq ido-use-filename-at-point 'guess)
(setq ido-create-new-buffer 'always)
(setq-default confirm-nonexistent-file-or-buffer nil)
(ido-mode 1)

;; Disable backup files
(setq make-backup-files nil)

;; Display matching parens
(require 'paren)
(setq show-paren-delay 0)
(show-paren-mode 1)

;; Initial scratch message
(setq initial-scratch-message ";; my heart is made of gravy")

;; UI Config
(tool-bar-mode -1)	       ;; Remove toolbar icons
(menu-bar-mode -1)	       ;; Remove standard menu bar
(scroll-bar-mode -1)	       ;; Remove scroll bars
(fringe-mode 0)		       ;; Skinny borders
(setq inhibit-splash-screen t) ;; Suppress splash screen


;; Custom key configs
(global-set-key (kbd "C-c u") 'overwrite-mode) ;; Toggle overwrite-mode



