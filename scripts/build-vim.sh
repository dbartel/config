#!/bin/sh

if [ -d "$(pwd)/vim" ]; then
    rsync -v ./vim/.vimrc ~/.vimrc;
else
    echo "no vim config, nothing to do"
    exit 0
fi
