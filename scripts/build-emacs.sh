#!/bin/sh

if [ -d "$(pwd)/emacs" ]; then
    cd "$(pwd)/emacs" || exit 1;
    emacs --script ./scripts/install-config.el
    cd - || exit 1;
else
    echo "No emacs config, nothing to do"
    exit 0
fi
