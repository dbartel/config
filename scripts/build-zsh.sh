#!/bin/sh

if [ -d "$(pwd)/zsh" ]; then
    ln -s -v `pwd`/zsh/packages ~/.config/zsh/packages
    rsync -v ./zsh/config.zsh ~/.zshrc
else
    echo "no zsh config, nothing to do"
    exit 0
fi
