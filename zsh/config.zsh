# Setup global variables
export ZSH_PACKAGES="~/.config/zsh/packages"

# Load zgen - zsh plugin manager
source ~/.config/zsh/packages/zgen/zgen.zsh

# Timestamp commands in history file
setopt extended_history

# Share history between terminals
setopt share_history

# Use ; as prompt for easy copy/paste
NEWLINE=$'\n'
PROMPT="%~${NEWLINE};"

# external packages

if ! zgen saved; then
    ## zsh-autosuggestions
    zgen load zsh-users/zsh-autosuggestions

    ## zsh-completions
    zgen load zsh-users/zsh-completions

    ## zsh-syntax-highlighting - must be last
    zgen load zsh-users/zsh-syntax-highlighting

    # save init script
    zgen save
fi

source ~/.config/zsh/local.zsh
